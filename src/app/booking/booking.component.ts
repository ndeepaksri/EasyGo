import { Component, OnInit } from '@angular/core';
import {
  minBaseFare, taxPercentage, baseDistance,
  baseDistanceFare, baseWaitingFare, baseWaitingTime,
  extraDistanceFare, extraWaitingFare
} from '../app.component';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  minBaseFare: number;
  taxPercentage: number;

  baseDistance: number;
  baseDistanceFare: number;
  extraDistanceFare: number;

  baseWaitingTime: number;
  baseWaitingFare: number;
  extraWaitingFare: number;

  actualDistance: number;
  actualWaitingTime: number;

  totalDistanceFare: number;
  totalWaitingFare: number;
  totalTax: number;
  totalEstimate: number;

  constructor() { 
    this.minBaseFare = minBaseFare;
    this.taxPercentage = taxPercentage;

    this.baseDistance = baseDistance;
    this.baseDistanceFare = baseDistanceFare;
    this.extraDistanceFare = extraDistanceFare;

    this.baseWaitingTime = baseWaitingTime;
    this.baseWaitingFare = baseWaitingFare;
    this.extraWaitingFare = extraWaitingFare;
  }

  ngOnInit() {
    this.actualDistance = 0.0;
    this.actualWaitingTime = 0;

    this.totalDistanceFare = 0.0;
    this.totalWaitingFare = 0.0;
    this.totalTax = 0.0;
    this.totalEstimate = 0.0;
  }

  onDistanceChange(distance: number) {
    this.actualDistance = distance;
    console.log(this.actualDistance);
    this.computeFare();

  }

  onTimeChange(time: number) {
    this.actualWaitingTime = time;
    console.log(this.actualWaitingTime);
    this.computeFare();

  }

  computeFare() {
    debugger;
    var extraDistance: number;
    var extraWaitingTime: number;
    if (this.actualDistance > this.baseDistance) {
      extraDistance = (this.actualDistance - this.baseDistance);
      this.totalDistanceFare = (Math.round(this.baseDistance * this.baseDistanceFare) + Math.round(extraDistance * this.extraDistanceFare));
    }
    else {
      this.totalDistanceFare = (Math.round(this.actualDistance * this.baseDistanceFare));
    }

    if (this.actualWaitingTime > this.baseWaitingTime) {
      extraWaitingTime = (this.actualWaitingTime - this.baseWaitingTime);
      this.totalWaitingFare = (Math.round(this.baseWaitingTime * this.baseWaitingFare) + Math.round(extraWaitingTime * this.extraWaitingFare));
    }
    else {
      this.totalWaitingFare = (Math.round(this.actualWaitingTime * this.baseWaitingFare));
    }

    var fareWithoutTax = (this.minBaseFare + this.totalDistanceFare + this.totalWaitingFare);

    this.totalTax = (Math.round(fareWithoutTax * this.taxPercentage) / 100);

    this.totalEstimate = fareWithoutTax + this.totalTax;
  }
}
