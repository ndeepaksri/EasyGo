import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { BookingComponent } from './booking/booking.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';

const appRoutes: Routes = [
  { path: 'booking', 
    component: BookingComponent,
    data: { title: 'Easy Go! - Booking' }
  },
  { path: 'admin', 
    component: AdminComponent,
    data: { title: 'Easy Go!' }
  },
  { path: '', 
    component: LoginComponent,
    data: { title: 'Easy Go!' }
  },
];
@NgModule({
  declarations: [
    AppComponent,
    BookingComponent,
    LoginComponent,
    AdminComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
