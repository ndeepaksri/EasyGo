import { Component } from '@angular/core';
import { Router } from '@angular/router';


export var minBaseFare: number;
export var taxPercentage: number;
export var baseDistance: number;
export var baseDistanceFare: number;
export var extraDistanceFare: number;
export var baseWaitingTime: number;
export var baseWaitingFare: number;
export var extraWaitingFare: number;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor(private router: Router) { 
    minBaseFare = 50.0;
    taxPercentage = 5;

    baseDistance = 10.0;
    baseDistanceFare = 10.0;
    extraDistanceFare = 8.0;

    baseWaitingTime = 10;
    baseWaitingFare = 0.0;
    extraWaitingFare = 1;
    
  }
  title = 'Easy Go!';
  
  ngOnInit() {
    
  }

  onSettingClick() {
    this.router.navigateByUrl('/admin');
  }
}
