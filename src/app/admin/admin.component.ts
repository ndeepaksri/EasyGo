import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { minBaseFare, taxPercentage, baseDistance, 
          baseDistanceFare, baseWaitingFare, baseWaitingTime, 
          extraDistanceFare, extraWaitingFare } from '../app.component';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  minBaseFare: number;
  taxPercentage: number;

  baseDistance: number;
  baseDistanceFare: number;
  extraDistanceFare: number;

  baseWaitingTime: number;
  baseWaitingFare: number;
  extraWaitingFare: number;

  constructor(private router: Router) {
    this.minBaseFare = minBaseFare;
    this.taxPercentage = taxPercentage;

    this.baseDistance = baseDistance;
    this.baseDistanceFare = baseDistanceFare;
    this.extraDistanceFare = extraDistanceFare;

    this.baseWaitingTime = baseWaitingTime;
    this.baseWaitingFare = baseWaitingFare;
    this.extraWaitingFare = extraWaitingFare;
   }

  ngOnInit() {
  }

  updateBaseClick() {
    this.router.navigateByUrl('/booking');
  }

}
